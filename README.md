# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Author: Blayne Goring

Contact address: Bgoring@uoregon.edu

This software makes and runs a python file named hello.py that prints nothing more and nothing less than "Hello world".
